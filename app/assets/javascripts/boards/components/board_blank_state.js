/* global ListLabel */

import Cookies from 'js-cookie';

const Store = gl.issueBoards.BoardsStore;

export default {
  template: `
    <div class="board-blank-state">
      <p>
        一键添加以下默认列表到你的问题看板中：
      </p>
      <ul class="board-blank-state-list">
        <li v-for="label in predefinedLabels">
          <span
            class="label-color"
            :style="{ backgroundColor: label.color }">
          </span>
          {{ label.title }}
        </li>
      </ul>
      <p>
        开启默认列表集可以最大程度地正确利用你的看板。
      </p>
      <button
        class="btn btn-create btn-inverted btn-block"
        type="button"
        @click.stop="addDefaultLists">
        添加默认列表集
      </button>
      <button
        class="btn btn-default btn-block"
        type="button"
        @click.stop="clearBlankState">
        没关系，我将自己创建列表
      </button>
    </div>
  `,
  data() {
    return {
      predefinedLabels: [
        new ListLabel({ title: '待办', color: '#F0AD4E' }),
        new ListLabel({ title: '进行中', color: '#5CB85C' }),
      ],
    };
  },
  methods: {
    addDefaultLists() {
      this.clearBlankState();

      this.predefinedLabels.forEach((label, i) => {
        Store.addList({
          title: label.title,
          position: i,
          list_type: 'label',
          label: {
            title: label.title,
            color: label.color,
          },
        });
      });

      Store.state.lists = _.sortBy(Store.state.lists, 'position');

      // Save the labels
      gl.boardService.generateDefaultLists()
        .then((resp) => {
          resp.json().forEach((listObj) => {
            const list = Store.findList('title', listObj.title);

            list.id = listObj.id;
            list.label.id = listObj.label.id;
            list.getIssues()
              .catch(() => {
                // TODO: handle request error
              });
          });
        })
        .catch(() => {
          Store.removeList(undefined, 'label');
          Cookies.remove('issue_board_welcome_hidden', {
            path: '',
          });
          Store.addBlankState();
        });
    },
    clearBlankState: Store.removeBlankState.bind(Store),
  },
};
